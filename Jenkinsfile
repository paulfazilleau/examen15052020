#!/usr/bin/env groovy

node {

    stage('Checkout') {
        checkout scm

        def packageJSON = readJSON file: 'package.json'
        def packageJSONVersion = packageJSON.version
        
        if (packageJSONVersion) {
            echo "Build version ${packageJSONVersion}"
        }
    }

    stage("Install") {
        withEnv(["PATH+NODE=${tool 'NodeJS'}/bin"]) {
            sh "npm install"
        }    
    }
    
    stage("Build") {
        withEnv(["PATH+NODE=${tool 'NodeJS'}/bin"]) {
            sh "npm run build-ci"
            archiveArtifacts artifacts: 'dist/*'
        }    
    }

    stage("Test") {
        withEnv(["PATH+NODE=${tool 'NodeJS'}/bin"]) {
            if(env.WITH_COVERAGE == "true") {
                sh "npm run test-ci"
                archiveArtifacts artifacts: 'coverage/**/*'
            } else {
                sh "npm run test-ci-without-coverage"
            }
        }
    }
}
